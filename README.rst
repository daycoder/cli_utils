
CLI Utils
========================================

Helpers for Command line interfaces.

Includes a function to get time (get_time) and a class (Menu)
for constructing menus.
